package net.tajzich.mt;

import net.tajzich.mt.domain.User;
import net.tajzich.mt.service.MultitenantService;
import net.tajzich.mt.singleton.ThreadContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {

    public static void main(String args[]) {
        final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        final MultitenantService multitenantService = context.getBean(MultitenantService.class);

        Runnable mt1 = new Runnable() {
            @Override
            public void run() {

               ThreadContext.getInstance().set("mt1");

                User user = new User();
                user.setId(1L);
                user.setName("mt1");

                multitenantService.deleteUsers();
                multitenantService.save(user);
            }
        };

        Runnable mt2 = new Runnable() {
            @Override
            public void run() {
                ThreadContext.getInstance().set("mt2");


                User user = new User();
                user.setId(1L);
                user.setName("mt2");

                multitenantService.deleteUsers();
                multitenantService.save(user);
            }
        };

       new Thread(mt1,"mt1-thread").start();
       new Thread(mt2,"mt2-thread").start();
        try {
            Thread.sleep(1000 *18);

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}