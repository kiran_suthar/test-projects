package net.tajzich.mt.singleton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: vtajzich
 * Date: 8/21/13
 */

public class ThreadContext
{
    private static volatile ThreadLocal<String> instance = null;

    private ThreadContext() {}

    public static ThreadLocal<String> getInstance()
    {
        if (instance == null)
        {
            synchronized (ThreadContext.class){
                if(instance == null){
                    instance = new ThreadLocal<String>();
                }
            }

        }
        return instance;
    }


}

