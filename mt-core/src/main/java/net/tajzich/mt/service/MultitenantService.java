package net.tajzich.mt.service;

import net.tajzich.mt.domain.User;
import net.tajzich.mt.singleton.ThreadContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vtajzich
 * Date: 8/21/13
 */
@Service
public class MultitenantService {

    private SessionFactory sessionFactory;


    public void save(User user) {
        Session session = beginSession();
        session.save(user);
        endSession(session);
    }

    public User getUser(Long id) {
        Session session = beginSession();
        User user = (User) session.get(User.class, id);
        endSession(session);
        return user;
    }

    public List<User> getUsers() {
        Session session = beginSession();
        List<User> users = session.createQuery("from User").list();
        endSession(session);
        return users;
    }

    public void deleteUsers()
    {
        Session session = beginSession();
        Query query = session.createQuery("delete from User");
        query.executeUpdate();
        endSession(session);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session beginSession() {

        System.out.println("tenantId is :"+ThreadContext.getInstance().get());
        Session session = sessionFactory.withOptions().tenantIdentifier(ThreadContext.getInstance().get()).openSession();
        session.beginTransaction();

        return session;
    }

    public void endSession(Session session){
        session.getTransaction().commit();
        session.close();
    }
}